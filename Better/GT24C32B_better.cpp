/**
 * @file GT24C32B.cpp
 *
 * This is a C++ library for the Arduino framework for the GT24C32B, a 32K EEPROM. It has a page size of 32 bytes.
 * It communicates data over I2C so only a connection to SDA and SCL is required.
 * The default configuration for the 3 address bits A2..A0 is 0 (grounded).
 * 
 * Version 1.0.
 * 07 July 2020
 * 
 * Written by Dr. ing. Yannick Verbelen at Bristol University
 * Creative Commons Attribution Share-Alike 4.0 (CC-BY-SA 4.0). All text above must be included in any redistribution.
 * 
 */

#include "GT24C32B_better.h"

/**
 * Checks if the EEPROM is busy with a write or read operation.
 * 
 * @return true if the EEPROM is busy, false if ready for read/write operations.
 */

void GT24C32B::setDeviceIDAndAddress(uint8_t deviceAddr){
  // device has 3 configurable bytes, so if this value is smaller than 8 dec then assume addr bits,
  // otherwise assume full 7 bit address
  if (deviceAddr < 8)    {
    device_address = GT24C32B_ADDR + deviceAddr;
    device_id_address = GT24C32B_ID_PAGE_ADDR + deviceAddr;
  }
  else {
    device_address = deviceAddr;
    device_id_address = GT24C32B_ID_PAGE_ADDR + (device_address - GT24C32B_ADDR);
  }
  
  if ((deviceAddr > 0x07) && (deviceAddr < 0x50) || deviceAddr > 0x57){
    // invalid values, default to 0x50
    device_address = GT24C32B_ADDR;
    device_id_address = GT24C32B_ID_PAGE_ADDR;
  }
}

/**
 * Initialize an EEPROM device at the specified I2C address.
 * 
 * @param deviceAddr The I2C address of the device. Either specify full 7 bit hexadecimal address, or a value between 0 and 7 corresponding to the 3 configurable address bits.
 */
GT24C32B::GT24C32B(uint8_t adr){
  setDeviceIDAndAddress(adr);
  Wire.begin(); // initialise I2C bus
}
  

/**
 * Initialize the EEPROM device at the default address (all configurable address bits low).
 */
GT24C32B::GT24C32B(){
  setDeviceIDAndAddress(GT24C32B_ADDR);
  Wire.begin(); // initialise I2C bus
}


bool GT24C32B::busy() const {
  // implement ACK polling as described on page 8 of the datasheet, §5.9.3
  Wire.beginTransmission(0x50);
  return (Wire.endTransmission() != 0);
}

/**
 * Reads a value from memory at the specified address
 * 
 * @param addr The target address to be read
 * @return The retrieved value
 */
uint8_t GT24C32B::read(uint16_t addr) const{
  if (addr > 0x0FFF) // only 4096 bytes in the address space
    return 0;

  while (busy());
  // wait until it's available for a new read operation

  Wire.beginTransmission(device_address);
  Wire.write(highByte(addr));
  Wire.write(lowByte(addr));
  Wire.endTransmission();

  Wire.requestFrom(device_address, 1);
  if (Wire.available()){
    return Wire.read();
  }
  else{
    return 0;
  }
}

/**
 * Reads the entire EEPROM memory space.
 *
 * @return a byte array with length 1024 containing the data.
 */
const std::array<uint8_t, MEM_SIZE>&  GT24C32B::read() const{
  static std::array<uint8_t, MEM_SIZE> mem;

  while (busy()); // wait until it's available for a new read operation

  // request data from address 0
  Wire.beginTransmission(device_address);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();

  // clear the buffer
  // this is wrong use of sizeof...
  //for (uint16_t n = 0; n < sizeof(mem); n++)
  mem.fill(0);
    
  // requestFrom() doesn't support reading 4096 bytes, so read it as 32 chunks of 128 bytes instead:
  // this is a rather ugly contruct, find a nicer way, please!
  uint16_t i = 0;
  for (uint8_t z = 0; z < (MEM_SIZE / 128); z++) {
    Wire.requestFrom(device_address, 128); // request block of 128 bytes
    while (Wire.available()) {
      mem[i++] = Wire.read();
    }
    delay(1);
  }
  return mem;  // return a ref to a const object!
}

/**
 * Reads a value from the specified ID page address.
 * 
 * @param addr The address in the ID page, must be a value between 0 and 31.
 * @return The data byte read from the ID page.
 */
uint8_t GT24C32B::readID(uint8_t addr) const{
  if (addr > 0x20) // ID page address space limited to 32 bytes
    return 0;

  while (busy());
  // wait until it's available for a new read operation

  Wire.beginTransmission(device_id_address);
  Wire.write(0);
  Wire.write(addr);
  Wire.endTransmission();

  Wire.requestFrom(device_id_address, 1);
  if (Wire.available())
    return Wire.read();
  else
    return 0;
}

const std::array<uint8_t, PAGE_SIZE>& GT24C32B::readIDPage(void) const{
  static std::array<uint8_t, PAGE_SIZE> idPage;
  while (busy());
  // wait until it's available for a new read operation

  Wire.beginTransmission(device_id_address);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();

  Wire.requestFrom(device_id_address, 32); // request the entire ID page
  idPage.fill(0);

  auto it = idPage.begin();
  while (Wire.available()){
    *it++ = Wire.read();
  }

  return idPage;
}


const std::array<uint8_t, 1024> & GT24C32B::readBlock(uint8_t block) const{
  static std::array<uint8_t, 1024> blockbuf;
  while (busy());
  // wait until it's available for a new read operation

  Wire.beginTransmission(device_address);
  uint16_t addr = block * 1024;
  Wire.write(highByte(addr));
  Wire.write(lowByte(addr));
  Wire.endTransmission();

  blockbuf.fill(0);

  auto it = blockbuf.begin();
  for (uint8_t z = 0; z < (1024 / 128); z++){
    Wire.requestFrom(device_address, 128);
    while (Wire.available()) {
      *it++ = Wire.read();
    }
  }
  return blockbuf;
}

/**
 * Write a byte to a specified memory address.
 * 
 * @param addr The address where the byte will be stored
 * @param value The data to be written
 */
void GT24C32B::write(uint16_t addr, uint8_t value) const {
  if (addr > 0x0FFF) // only 4096 bytes in the address space
    return;
  
  while (busy());
  // wait until it's available for a new write operation

  Wire.beginTransmission(device_address);
  Wire.write(highByte(addr));
  Wire.write(lowByte(addr));
  Wire.write(value);
  Wire.endTransmission();
}

/**
 * Write an entire 32 byte page at once. Pages start at addresses that are a multiple of 32.
 * 
 * @param addr The address of the page in memory
 * @param values The data of the page
 */
// use a reference to arg
void GT24C32B::writePage(uint16_t addr, const std::array<uint8_t, 32> &values) const{
  // only 4096 bytes in the address space, minus 32 bytes page size
  if (addr > (0x0FFF - 32)) 
    return;

  // page start addresses are modulo 32
  if ((addr % 32) != 0)
    return;

  while (busy());
  // wait until it's available for a new read operation

  Wire.beginTransmission(device_address);
  Wire.write(highByte(addr));
  Wire.write(lowByte(addr));
  for (auto it = values.begin(); it != values.end(); it++) {
    Wire.write(*it);
  }
  //for (uint8_t i = 0; i < sizeof(values); i++)  // this is wrong! sizeof will return memory size not nb of elts.
  //Wire.write(values[i]);
  Wire.endTransmission();
}

/**
 * Writes a value to the specified location in the 32 byte ID page.
 * 
 * @param addr The address where data is written
 * @param value The data to be written
 */
void GT24C32B::writeID(uint8_t addr, uint8_t value)const{
  if (addr > 0x20) // ID page address space limited to 32 bytes
    return;
  
  while (busy());
  // wait until it's available for a new write operation
  
  Wire.beginTransmission(device_id_address);
  Wire.write(0);
  Wire.write(addr);
  Wire.write(value);
  Wire.endTransmission();
}

void GT24C32B::writeIDPage(std::array<uint8_t, PAGE_SIZE> IdPageData) const{
  while (busy());
  // wait until it's available for a new read operation

  Wire.beginTransmission(device_id_address);
  Wire.write(0);
  Wire.write(0);
  // incorrect use of sizeof!
  //for (uint8_t i = 0; i < IdPageData.size(); i++)
  for (auto it = IdPageData.begin(); it != IdPageData.end(); it++) {
    Wire.write(*it);
  }
  Wire.endTransmission();
}

/**
 * Clears all memory contents.
 * 
 */
void GT24C32B::clear() const{
  while (busy()); // wait until it's available for a new R/W operation

  std::array<uint8_t, PAGE_SIZE> page;
  page.fill(0xFF);

  for (uint16_t p = 0; p < (MEM_SIZE / PAGE_SIZE); p++)
    writePage(p * PAGE_SIZE, page);
}
