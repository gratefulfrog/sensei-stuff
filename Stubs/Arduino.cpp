// Arduino.cpp stub

#include "Arduino.h"

uint8_t highByte(uint16_t inVal){
  return (uint8_t)((inVal>>8)&0xFF);
}

uint8_t lowByte(uint16_t inVal){
  return (uint8_t)(inVal&0xFF);
}

uint16_t  analogRead(uint8_t i){
  return (uint16_t)i;
}

void  pinMode(uint8_t,uint8_t){}

const uint8_t A0 = 0;


#include <chrono>
#include <iostream>

using namespace std;
using namespace chrono;

void delay(int ms) {
  auto begin = high_resolution_clock::now();
  while(duration_cast<milliseconds>(high_resolution_clock::now()-begin).count() < ms);
}  

void Serial_::print(const char* const c)const{
  cout << c;
}
void Serial_::print(int i) const{
  cout << i;
}
void Serial_::println(const char* const c)const{
  cout << c << endl;
}
void Serial_::println(int i) const{
  cout << i << endl;
}
void Serial_::println() const{
  cout << endl;
}

Serial_  Serial = Serial_();
