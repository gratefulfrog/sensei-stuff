// Wire.h stub

#ifndef WIRE_H
#define WIRE_H

#include <cstdint>

class Wire_{
 public:
  void begin();
  void beginTransmission(uint8_t);
  bool endTransmission();
  void write(uint8_t);
  void requestFrom(uint8_t, uint8_t);
  bool available();
  uint8_t read();
  
};

extern Wire_ Wire;

#endif
