// Arduino.h stub

#ifndef ARDUINO_H
#define ARDUINO_H

#include <cstdint>

#define INPUT (1)

extern uint8_t   highByte(uint16_t);
extern uint8_t   lowByte(uint16_t);
extern void      delay(int);
extern uint16_t  analogRead(uint8_t);
extern void      pinMode(uint8_t,uint8_t);

extern const uint8_t A0;



class Serial_ {
 public:
  void begin(int)const {};
  void print(const char* const) const;
  void print(int) const;

  void println(const char* const) const;
  void println(int) const;
  void println() const;

};
extern Serial_  Serial;


#endif
