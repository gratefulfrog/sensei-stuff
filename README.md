Stuff to help Sensei program like a Wizard!

# Demo
I made a little demo to show some of the features of the std::array class.
This can be tested by
```
$ cd Demo
$ g++ g++ ThingArray5.cpp Thing.cpp
$ ./a.out
```

You will see the results of a race between 5 things. First to reach 1000 is the winner!

# Stubs

In the Better/Stubs directory, I have created a few little stubs for arduino stuff.

They can be tested by:
```
$ cd Stubs
$ g++ arduinoTest.cpp Arduino.cpp
$ ./a.out
```
# Charge Computing
Some more stuff that should help see the path of enlightenment!

```
$ cd ChargeComputing/Better
$ g++ *.cpp ../../Stubs/Arduino.cpp
$ ./a.out
```

