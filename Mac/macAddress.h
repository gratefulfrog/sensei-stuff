// macAddress.h
// Definition of the Thing Class

#ifndef MAC_ADDRESS_H
#define MAC_ADDRESS_H

#include <iostream>
#include <cstdint>

using namespace std;

#define ADDRESS_LENGTH (6)


// define a typedef to make the synatx easier to understand!!
// the type 'macAddressVector' is a vector of uint8_t of length ADDRESS_LENGTH
typedef uint8_t macAddressVector[ADDRESS_LENGTH];

class MacAddress{
private:
  macAddressVector address;
  
public:
  MacAddress(macAddressVector &vec);
  // get the vector!
  const macAddressVector& getAddress() const;

  friend ostream &operator<<(ostream &output, const MacAddress&);
};
	  
#endif
