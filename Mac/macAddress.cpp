// macAddress.cpp
// Definition of the Thing Class

#include "macAddress.h"

#define TEST

MacAddress::MacAddress(macAddressVector &vec){
  for(uint8_t i=0;i<ADDRESS_LENGTH;i++){
    address[i]=vec[i];
  }
}

const macAddressVector& MacAddress::getAddress() const{
  return address;
}

ostream &operator<<(ostream &output, const MacAddress& adr){
  for (uint8_t i=0;i<ADDRESS_LENGTH-1;i++){
    output << (int)adr.address[i] << ':';  // need to cast to get representation for std out!
  }
  output << (int)adr.address[ADDRESS_LENGTH-1] << endl;
  return output;
}
  
#ifdef TEST

// simulate the function from your code
uint8_t* WiFi_BSSID(uint8_t input){
  static uint8_t vec[ADDRESS_LENGTH];
  for (uint8_t i=0; i< ADDRESS_LENGTH;i++){
    vec[i] = min(input*i,255); // max uint8_t
  }
  return vec;
}


int main(){
  // create a vector to be used as a mac address
  uint8_t myAdr[ADDRESS_LENGTH];
  
  //initialize the vector
  for (uint8_t i= 0; i< ADDRESS_LENGTH;i++){
    myAdr[i] = i*10;
  }
  
  // create an instance of MacAddress using the vector as initializer
  MacAddress adrInstance = MacAddress(myAdr);

  // print it to stdout!
  cout << "first print\n" << adrInstance << "ok!" << endl;

  // create it using the function to initialize
  uint8_t *res = WiFi_BSSID(3);

  for (uint8_t i= 0; i< ADDRESS_LENGTH;i++){
    myAdr[i] = res[i];
  }
  
  adrInstance = MacAddress(myAdr);

  cout << "Second print\n" << adrInstance << "ok!" << endl;

  cout << "Third print\n";
  const macAddressVector &ref   = adrInstance.getAddress();
  for (uint8_t i= 0; i< ADDRESS_LENGTH-1;i++){
    cout << (int)ref[i] << ':';
  }
  cout << (int)ref[ADDRESS_LENGTH-1] <<endl <<"ok" << endl;
  
  return 0;
}

/* results
 * $ g++ macAddress.cpp
 * $ ./a.out 
 * first print
 * 0:10:20:30:40:50
 * ok!
 * Second print
 * 0:3:6:9:12:15
 * ok!
 * Third print
 * 0:3:6:9:12:15
 * ok
 * $ 
 */

#endif
  
