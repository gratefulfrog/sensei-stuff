// ThingArray5.h
// Definition of the ThingArray Class

#ifndef THING_ARRAY_5_H
#define THING_ARRAY_5_H

#include <array>
#include <string>
#include "Thing.h"

using namespace std;

class ThingArray5{
private:
  array<Thing*,5> at;
  static array<string,5> nameArray;
  const static int raceEnd;  
  const static int raceStepLimit;

 public:
  ThingArray5();  // constructor
  void step();    // step each Thing
  void show() const; // show each Ting
  bool haveWinner() const; // test to see if a Thing has reached raceEnd
};
	  
#endif
