// Thing.cpp
// Definitions of the Thing Class

//#define TEST_THING

#include "Thing.h"

Thing::Thing(const string& s):name(s){
  x=0;
}
Thing::Thing(): name("Anonymous"){
  x=0;
}
const string& Thing::getName() const{
  return name;
}
int Thing::getX() const{
  return x;
}
void Thing::setX(int p){
  x=p;
}
void Thing::move(int deltaX){
  x +=deltaX;
}

ostream& operator<<( ostream &output, const Thing &T){ 
  output << "Thing : " << T.name << " \t@ : " << T.x;
  return output;            
}

#ifdef TEST_THING
int main(){
  Thing t1("Frank");
  cout << t1 << endl;
  t1.setX(27);
  cout << t1 << endl;
  t1.move(100);
  cout << t1 << endl;
  return 0;
}
#endif
