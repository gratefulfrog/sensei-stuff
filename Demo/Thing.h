// Thing.h
// Definition of the Thing Class

#ifndef THING_H
#define THING_H

#include <string>
#include <iostream>
#include <string>

using namespace std;

class Thing{
private:
  const string name;
  int x;  // the x position
  
public:
  Thing(const string&);
  Thing();
  const string& getName() const;
  int getX() const;
  void setX(int x);
  void move(int deltaX);
  
  friend ostream &operator<<(ostream &output, const Thing&);
};
	  
#endif
