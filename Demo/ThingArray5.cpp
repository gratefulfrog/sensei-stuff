// Thing.cpp
// Definitions of the Thing Class

#define TEST_THING_ARRAY

#include "ThingArray5.h"
#include <cstdlib>  // access to rand() function
#include <ctime>   // access to time() function


// the end value for the Thing race! (static)
const int ThingArray5::raceEnd  = 1000;
const int ThingArray5::raceStepLimit  = 10;

// an array of names for Things (static)
array<std::string,5> ThingArray5::nameArray={"John",
					     "Frank",
					     "Bill",
					     "Al",
					     "Sid"};
// Constructor:
// iterate over the member array and create the Things using the
// static array of names
ThingArray5::ThingArray5(){
  auto namesBegin = nameArray.begin();
  for (auto tIt = at.begin();
       tIt != at.end();
       tIt++){
    // note that tIt is of type pointer to that which is in the array,
    // since that which is in the array is a pointer to a Thing,
    // then the type of tIt is pointer to pointer to Thing
    *tIt =  new Thing(*namesBegin++);
  }
  // seed the random number generator
  srand (time(NULL));
}

// step each Thing a random amount
void ThingArray5::step(){
  for (auto tIt = at.begin();tIt != at.end(); tIt++){
    (*tIt)->move(rand()%10); // make a random step on [0,raceStepLimit[
  }
}

// show all the Things
void ThingArray5::show() const{
  for (auto tIt = at.begin();tIt != at.end(); tIt++){
    cout<< **tIt << endl;
  }
}

// return True if at least one of the Things has reached raceEnd
bool ThingArray5::haveWinner() const{
  for (auto tIt = at.begin();tIt != at.end(); tIt++){
    if ((*tIt)->getX() >=raceEnd){
      return 1;
    }
  }  
  return 0;
}

#ifdef TEST_THING_ARRAY
int main(){
  ThingArray5 ta = ThingArray5();
  while(!ta.haveWinner()){
    ta.step();
  }
  ta.show();
}
#endif
