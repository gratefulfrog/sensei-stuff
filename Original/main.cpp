/**
 * This is a sample script to demonstrate use of the GT24C32B EEPROM device driver.
 * 
 * Version 1.0.
 * 07 July 2020
 * 
 * Written by Dr. ing. Yannick Verbelen at Bristol University
 * Creative Commons Attribution Share-Alike 4.0 (CC-BY-SA 4.0). All text above must be included in any redistribution.
 * 
 */

#include <Arduino.h>
#include <GT24C32B.h> // header file for the GT24C32B

GT24C32B eeprom; // instance of the class

void setup() {
  Serial.begin(9600); Serial.println("\n");

  // Initialise the EEPROM for reading and writing.
  // Note: writing will NOT work if the write protect (WP) pin is high on the device.
  eeprom.begin();

  // (1) Write a byte to a specified address in the memory space
  const uint16_t addr = 335;
  const uint8_t value = 19;
  eeprom.write(addr, value);


  // (2) Read a byte from a specified address in the memory space
  uint8_t val = eeprom.read(addr);
  Serial.print("read value is "); Serial.println(val);


  // (3) Since the EEPROM is organised in 32 byte pages, it's possible to write 32 bytes at a time
  // Only addresses that are a multiple of the page size are valid (i.e 0, 32, 64, etc.)
  std::array<uint8_t, PAGE_SIZE> page;
  for (uint8_t i = 0; i < sizeof(page); i++)
    page[i] = i; // fill with dummy data
  eeprom.writePage(96, page);


  // (4) Read an entire block of 1 kiB (1024 bytes)
  // The argument is the block number, since the memory space is 4 kiB, it can be a value from 0 - 3.
  std::array<uint8_t, 1024> block;
  block = eeprom.readBlock(0);
  Serial.println("reading block");
  for (uint16_t i = 0; i < sizeof(block); i++)
  {
    Serial.print(block[i]); Serial.print(",");
    delay(1); // a delay is necessary to prevent the software watchdog from triggering a reset
  }
  Serial.println();

  // (5) Clear all contents in the memory space
  eeprom.clear();

  Serial.println("all done!\n");

  /* Notes:
     a) There is also a special page called the ID page, which is separate to the rest of the memory structure.
        It can be written and read with readId() and writeId() respectively, or readIdPage() and writeIdPage().
     b) Locking the ID page is not supported by the device driver because the locking is irreversible.
     c) The function read() returns a 4096 byte array with the entire memory contents.
        However this requires 4 kiB of RAM on the microcontroller to be allocated. On the ESP8266 this triggers a stack exception.
     d) The clear() function does not affect the ID page.
  */
}

void loop() {}