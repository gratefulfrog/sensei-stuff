/**
 * @file GT24C32B.h
 *
 * This is a C++ library for the Arduino framework for the GT24C32B, a 32K EEPROM. It has a page size of 32 bytes.
 * It communicates data over I2C so only a connection to SDA and SCL is required.
 * The default configuration for the 3 address bits A2..A0 is 0 (grounded).
 * 
 * Version 1.0.
 * 07 July 2020
 * 
 * Written by Dr. ing. Yannick Verbelen at Bristol University
 * Creative Commons Attribution Share-Alike 4.0 (CC-BY-SA 4.0). All text above must be included in any redistribution.
 * 
 */

#ifndef __GT24C32B_H__
#define __GT24C32B_H__
#endif

#include <Arduino.h>
#include <Wire.h>


#define GT24C32B_ADDR 0x50
#define GT24C32B_ID_PAGE_ADDR 0x58

#define MEM_SIZE 4096
#define PAGE_SIZE 32

class GT24C32B;

class GT24C32B {
    public:
        bool busy(void);
        void begin(uint8_t deviceAddr);
        void begin(void);

        void write(uint16_t addr, uint8_t value);
        uint8_t read(uint16_t addr);

        void writePage(uint16_t addr, std::array<uint8_t, PAGE_SIZE> values);

        std::array<uint8_t, MEM_SIZE> read();

        void writeID(uint8_t addr, uint8_t value);
        void writeIDPage(std::array<uint8_t, PAGE_SIZE> IdPageData);
        uint8_t readID(uint8_t addr);
        std::array<uint8_t, PAGE_SIZE> readIDPage(void);

        std::array<uint8_t, 1024> readBlock(uint8_t block);

        void clear();


    private:
        uint8_t device_address;
        uint8_t device_id_address;

};