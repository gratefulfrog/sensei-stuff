/**
 * @file batterymonitor.h
 *
 * This is a C++ library for the Arduino framework to estimate the state of charge (SoC) of a Li-ion battery
 * based on intrapolation of the cell voltage. This only works for low discharge currens (< 0.2C)
 * and single cells with a maximum voltage of 4.2V and cut-off voltage of 3.0V.
 * The default voltage divider is 2.7kΩ / 10kΩ.
 * 
 * Version 1.0.
 * 17 July 2020
 * 
 * Written by Dr. ing. Yannick Verbelen at Bristol University
 * Creative Commons Attribution Share-Alike 4.0 (CC-BY-SA 4.0). All text above must be included in any redistribution.
 * 
 */

#ifndef __BATT_MONITOR__
#define __BATT_MONITOR__


#include "../../Stubs/debug.h"

#ifdef STUB
#include "../../Stubs/Arduino.h"
#else
#include <Arduino.h>
#endif

#include <array>

// voltage divider fraction on the ADC input pin
#define FRAC (2.7 / (2.7 + 10))
// ADC resolution, expressed as 2^nbits
#define ADC_RESOLUTION (1024)

class batterymonitor {
    public:
        batterymonitor(uint8_t pin);
        float voltage(void) const;
        float charge(void) const;
        uint16_t remainingcharge(uint16_t maxCapacity) const;

    private:
        uint16_t readADC() const;
        float calculateVoltage(uint16_t AdcValue, float fraction) const;

        const uint8_t AdcPin;
        const std::array<float, 21> dischargeCurve;

};

#endif
