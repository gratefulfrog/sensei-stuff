/**
 * @file batterymonitor.cpp
 *
 * This is a C++ library for the Arduino framework to estimate the state of charge (SoC) of a Li-ion battery
 * based on intrapolation of the cell voltage. This only works for low discharge currens (< 0.2C)
 * and single cells with a maximum voltage of 4.2V and cut-off voltage of 3.0V.
 * The default voltage divider is 2.7kΩ / 10kΩ.
 * 
 * Version 1.0.
 * 17 July 2020
 * 
 * Written by Dr. ing. Yannick Verbelen at Bristol University
 * Creative Commons Attribution Share-Alike 4.0 (CC-BY-SA 4.0). All text above must be included in any redistribution.
 * 
 */

#include "batterymonitor.h"

/**
 * Creates an instance of the battery monitor for a battery attached to the ADC at the specified pin, with a voltage divider.
 * 
 * @param pin The ADC pin to which the battery is connected with a voltage divider.
 */
batterymonitor::batterymonitor(uint8_t pin): AdcPin(pin),
					     dischargeCurve({4.15,
						   4.05,
						   4.0,
						   3.97,
						   3.94,
						   3.88,
						   3.82,
						   3.79,
						   3.76,
						   3.74,
						   3.72,
						   3.71,
						   3.7,
						   3.7,
						   3.7,
						   3.69,
						   3.68,
						   3.67,
						   3.64,
						   3.49,
						   3.0}){
  pinMode(AdcPin, INPUT);
}

/**
 * Reads a value from the ADC.
 * 
 * @return An integer value between 0 and 2^ADC_RESOLUTION - 1 representing the input voltage.
 */
uint16_t batterymonitor::readADC() const{
    return analogRead(AdcPin);
}

/**
 * Calculates the input voltage based on an ADC input, its resolution, and a voltage divider fraction.
 * 
 * @param AdcValue A raw ADC value expressed as an integer between 0 and 2^ADC_RESOLUTION - 1.
 * @param fraction The voltage divider fraction expressed as R1 / (R1 + R2).
 * @return The input voltage.
 */
float batterymonitor::calculateVoltage(uint16_t AdcValue, float fraction) const{
    return (AdcValue / fraction / ADC_RESOLUTION);
}

/**
 * Returns the battery voltage.
 * 
 * @return The battery voltage in V.
 */
float batterymonitor::voltage() const{
    return calculateVoltage(readADC(), FRAC);
}

/**
 * Returns the remaining charge fraction of the battery.
 * 
 * @return The remaining charge of the battery expressed as a percentage of its nominal capacity.
 */    
float batterymonitor::charge() const{
  // read battery voltage
  float v = voltage();
  
  // find the closest matching voltage in the discharge curve:
  for (uint8_t n = 0; n < (dischargeCurve.size() - 1); n++){
    float vi = dischargeCurve[n];
    if (v >= vi)
      return 100.0;
    
    float vj = dischargeCurve[n + 1];
    if (v <= vi)  {
      // extrapolate to the next value
      float vdiff = vi - vj;
      float cdiff = 100 / dischargeCurve.size();
      
      float cfrac = (vi - v) / (vdiff) * cdiff; // linear extrapolation
      
      float cap = 100 * ((dischargeCurve.size() - n) / dischargeCurve.size()) - cfrac; // calculate as fraction of 100%
      
      return cap;
      break;
    }
  }
}

/**
 * Returns the remaining charge of the battery.
 * 
 * @param maxCapacity The nominal maximum capacity of the battery expressed in mAh.
 * @return The remaining charge of the battery expressed in mAh.
 */
uint16_t batterymonitor::remainingcharge(uint16_t maxCapacity) const{
    return (uint16_t) (maxCapacity * charge() / 100);
}

