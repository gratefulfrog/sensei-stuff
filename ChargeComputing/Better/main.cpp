/**
 * This is a sample script to demonstrate use of the battery monitoring functionality.
 * 
 * Version 1.0.
 * 17 July 2020
 * 
 * Written by Dr. ing. Yannick Verbelen at Bristol University
 * Creative Commons Attribution Share-Alike 4.0 (CC-BY-SA 4.0). All text above must be included in any redistribution.
 * 
 */
#include "../../Stubs/debug.h"

#ifdef STUB
#include "../../Stubs/Arduino.h"
#else
#include <Arduino.h>
#endif

#include "batterymonitor.h"


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  batterymonitor battery(A0); // battery attached to analog pin A0, with voltage divider

  // estimate battery charge and remaining capacity based on battery voltage.
  Serial.print("\nBattery charge: ");
  Serial.print(battery.charge());
  Serial.println("%");

  Serial.print("Battery voltage: ");
  Serial.print(battery.voltage());
  Serial.println(" V");

  Serial.print("Remaining capacity: ");
  Serial.print(battery.remainingcharge(2000));
  Serial.println(" mAh");
}

void loop() {
  // put your main code here, to run repeatedly:
}


#ifdef STUB
int main (){
  setup();
}
#endif
