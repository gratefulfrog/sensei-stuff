#include <iostream>
using namespace std;

    
/* run this code!
 * $ g++ sizeIT.cpp
 * $ ./a.out
 */

int *getArray(){
  static int ar[] = {1,2,3,4,5,6,7,8};
  return ar;
}

int main(){
  int x = 10;
  // here we see the size of an int
  cout << "sizeof(x) : " << sizeof(x)  << endl;
  cout << "sizeof(int) : " << sizeof(int)  << endl;

  int *iPtr = &x;
  // here we see the size of the address of an int
  cout << "sizeof(iPtr) : " << sizeof(iPtr)  << endl;
  cout << "sizeof(int*) : " << sizeof(int*)  << endl;
  
  int XTen[10];
  // here we see various sizes relating to the array of 10 ints
  // this is the size of the array
  cout << "sizeof(XTen) : " << sizeof(XTen)  << endl;
  cout << "sizeof(int[10]) : " << sizeof(int[10])  << endl;
  // this is the sizeo of the object pointed by indiection on the array, i.e. the sizeof the first int 
  cout << "sizeof(*XTen) : " << sizeof(*XTen)  << endl;

  int * ptr =getArray();
  // here all we know is that that getArray returns the address of an int,  so sizeof return the size of the address of an int!
  cout << "sizeof(ptr) : " << sizeof(ptr)  << endl;
  
  // this is the size of the first element of the array pointed by returned value, i.e. the sizeof(int)!
  cout << "sizeof(*ptr) : " << sizeof(*ptr)  << endl;  
  
}

